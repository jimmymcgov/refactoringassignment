package refactor;

import org.junit.Test;

import javax.swing.*;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class EmployeeDetailsTest {




    ArrayList<Employee> createEmployees(){
        ArrayList<Employee> empList = new ArrayList<>();
        Employee paddy = new Employee(1,"1234567P","Mathews","Paddy",'M',"Production",100000,true);
        Employee james = new Employee(2,"7654321J","McGovern","James",'M',"Production",50000,true);
        empList.add(paddy);
        empList.add(james);

        return empList;
    }

    @Test
    public void searchEmployeeById() throws Exception {
        ArrayList<Employee> employees = createEmployees();
        JTextField searchField = new JTextField();
        searchField.setText("1");
        Employee employee = null;

        if(employees != null){
            for(Employee emp: employees){
                if (Integer.parseInt(searchField.getText().trim()) == emp.getEmployeeId()) {
                    employee = emp;
                }
            }
        }

        assertNotNull(employee);
    }

    @Test
    public void searchEmployeeBySurname() throws Exception {
        ArrayList<Employee> employees = createEmployees();
        JTextField searchField = new JTextField();

        searchField.setText("McGovern");
        Employee employee = null;

        if(employees != null){

            for(Employee emp: employees){
                if (searchField.getText().trim().equalsIgnoreCase(emp.getSurname()) ){
                    employee = emp;
                }
            }
        }
        assertNotNull(employee);
    }

}