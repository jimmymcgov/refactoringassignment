package refactor;

import javax.swing.*;

class SearchBySurnameDialog extends JDialog{
	private JButton search, cancel;
	private JTextField searchField;

	SearchBySurnameDialog(EmployeeDetails parent) {
        String filterBy = "Surname";
        setTitle("Search by Surname");
        setModal(true);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        SearchDialog searchDialog = new SearchDialog(parent, search, cancel, searchField, filterBy );
        JScrollPane scrollPane = new JScrollPane(searchDialog.searchPane());
        setContentPane(scrollPane);

        getRootPane().setDefaultButton(search);

        setSize(500, 190);
        setLocation(350, 250);
        setVisible(true);
	}

}
