package refactor;

import javax.swing.JOptionPane;
import javax.swing.text.*;

class JTextFieldLimit extends PlainDocument {
  private final int Limit;
  JTextFieldLimit(int limit) {
    super();
    this.Limit = limit;
  }

  public void insertString(int offset, String str, AttributeSet attr) throws BadLocationException {
    if (str == null)
      return;

    if ((getLength() + str.length()) <= Limit)
      super.insertString(offset, str, attr);
    else
    	JOptionPane.showMessageDialog(null, "For input " + Limit + " characters maximum!");
  }
}