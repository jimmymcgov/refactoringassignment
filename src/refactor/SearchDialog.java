package refactor;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import java.awt.*;
import java.awt.event.*;
class SearchDialog extends JDialog implements ActionListener {

        private EmployeeDetails parent;
        private JButton search, cancel;
        private JTextField searchField;
        private String searchType;

        SearchDialog(EmployeeDetails parent, JButton search, JButton cancel, JTextField searchField , String searchType) {
            this.parent = parent;
            this.search = search;
            this.cancel = cancel;
            this.searchField = searchField;
            this.searchType = searchType;
        }

        Container searchPane() {
            JPanel searchPanel = new JPanel(new GridLayout(3, 1));
            JPanel textPanel = new JPanel();
            JPanel buttonPanel = new JPanel();
            JLabel searchLabel;

            searchPanel.add(new JLabel("Search by "+searchType));

            textPanel.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
            textPanel.add(searchLabel = new JLabel("Enter "+searchType+":"));
            searchLabel.setFont(this.parent.Font1);
            textPanel.add(searchField = new JTextField(20));
            searchField.setFont(this.parent.Font1);
            searchField.setDocument(new JTextFieldLimit(20));

            buttonPanel.add(search = new JButton("Search"));
            search.addActionListener(this);
            search.requestFocus();

            buttonPanel.add(cancel = new JButton("Cancel"));
            cancel.addActionListener(this);

            searchPanel.add(textPanel);
            searchPanel.add(buttonPanel);

            return searchPanel;
        }

        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == search ) {
                if(searchType.equals("ID")) {
                    try {
                        this.parent.searchByIdField.setText(searchField.getText());
                        this.parent.searchEmployeeById();
                        dispose();
                    } catch (NumberFormatException num) {
                        searchField.setBackground(new Color(255, 150, 150));
                        JOptionPane.showMessageDialog(null, "Wrong ID format!");
                    }
                }else if(this.searchType.equals("Surname")){
                    this.parent.searchBySurnameField.setText(searchField.getText());
                    this.parent.searchEmployeeBySurname();
                    dispose();
                }
            }
            else if (e.getSource() == cancel)
                dispose();
        }

}